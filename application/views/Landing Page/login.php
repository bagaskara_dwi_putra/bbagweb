<!DOCTYPE html>
<html>
<head>
	<title>Login | Bagasweb</title>
	<link rel="icon"  href="<?= base_url('/img/B.png')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.css')?> ">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.grid.css')?> ">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css')?>">
</head>
<body class="body-login">
	<!-- login -->
	<div class="container">
		<div class="login">
			<div class="row d-flex flex-row align-items-center">
				<div class="d-none d-lg-block col-sm-12 col-lg-6 align-self-start">
					<img src="<?= base_url('/img/bg-login.png')?>" class="img-fluid"></img>
				</div>
				<div class="col2 col-sm-12 col-lg-6 py-4 px-4">
					<h3 class="text-center">Masuk ke Sistem</h3>
					<div class="row d-flex flex-row my-3">
						<div class="col-6">
							<a href=""><img src="<?= base_url('/img/facebook.png')?>" class="img-fluid float-right" width="30"></a>
						</div>
						<div class="col-6">
							<a href=""><img src="<?= base_url('/img/in.png')?>" class="img-fluid" width="30"></a>
						</div>
					</div>
					<p class="text-center mb-5 tlogin">Atau menggunakan akun email anda</p>
					<form>
						<input type="email" name="email" class="form-control input-login" placeholder="Masukkan Email Anda">
						<input type="password" name="password" class="form-control my-4 input-login" placeholder="Masukkan Password Anda">
						<center><a href="#" class="text-center font-weight-bold link-lupa">Lupa Password?</a></center>
						<center><button type="submit" class="btn btn-outline-dark font-weight-bold btn-masuk mt-5">Masuk</button></center>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- login -->

	<script src="js/jquery.slim.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	
</body>
</html>