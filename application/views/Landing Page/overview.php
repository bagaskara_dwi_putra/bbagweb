<!DOCTYPE html>
<html>
<head>
	<title>Landing Page | Bagasweb</title>
	<link rel="icon"  href="<?= base_url('/img/B.png')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.css')?> ">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.grid.css')?> ">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css')?>">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@200&display=swap" rel="stylesheet">
</head>
<body>
	<!-- navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm sticky-top">
		<div class="container">
		  <a class="navbar-brand" href="#"><img src="<?= base_url('img/Bbagweb.png') ?>" class="img-fluid" width="100"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto mr-2">
		      <li class="nav-item">
		        <a class="nav-link" href="#"><span class="twhite">Home</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link ml-2" href="#"><span class="twhite">About</span></a>
		      </li>
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle ml-2" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
		          <span class="twhite">Layanan</span>
		        </a>
		        <div class="dropdown-menu">
		          <a class="dropdown-item" href="#">Pengembang Situs Website</a>
		          <div class="dropdown-divider"></div>
		          <a class="dropdown-item" href="#">Pengembangan Aplikasi Website</a>
		          <div class="dropdown-divider"></div>
		          <a class="dropdown-item" href="#">Pengembangan Aplikasi Seluler</a>
		        </div>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link ml-2" href="#"><span class="twhite">Portofolio</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link ml-2" href="#"><span class="twhite">Kontak</span></a>
		      </li>
		    </ul>
		      <a href="<?= base_url('index.php/login')?>"><button class="btn btn-primary px-4 my-2 my-sm-0 font-weight-bold radius">LOGIN</button></a>
		  </div>
		</div>
	</nav>
	<!-- end navbar -->
	<!-- heri unit -->
	<section class="jumbotron py-0 px-0">
		<div class="full">
			<div class="container my-auto">
				<div class="row d-flex flex-column align-items-center"></div>
					<div class="col-sm-12 col-lg-6 mt-5">
						<h1 class="text-white sizing">Perusahaan <br>Pengembang Situs <br>Web, Aplikasi Web, <br>Aplikasi Seluler</h1>
					</div>
					<div class="col-sm-12 col-lg-6">
						<p class="text-white mt-1"><img src="<?= base_url('img/icon-laptop.png') ?>" width="50" class="img-fluid mb-2 mr-1"> <span class="font-weight-bold size">Bagaimana kami membantu anda?</span></p>
					</div>
					<div class="col-sm-12 col-lg-6">
						<p class="text-white">Jasa pengembangan software yang dapat disesuaikan dengan kebutuhan bisnis Anda, sehingga efektif dalam menemukan audiens yang potensial. Kami akan memproses dengan cepat dan tepat, didukung oleh tim yang berpengalaman dan profesional. Jadi, tunggu apa lagi?</p>
					</div>
					<div class="col-sm-12 col-lg-6">
						<a href="login.html" class="btn btn-outline-light px-4 mt-4">Pelajari Lebih Lanjut</a>
					</div>
				
			</div>
		</div>
	</section>
	<!-- end hero unit -->
	<!-- portofolio -->
	<section class="portofolio py-3 porto">
		<div class="container">
			<div class="row d-flex flex-row align-items-center">
				<div class="col-12">
					<h2 class="text-center">PORTOFOLIO</h2>
					<p class="text-center">Selama bertahun-tahun, kami telah membantu ratusan klien yang ingin memulai bisnis di dunia digital. Lihat beberapa karya terbaik kami.</p>
				</div>
			</div>
			<div class="row d-flex flex-row justify-content-center mt-4">
				<div class="col col-sm-12 col-lg-5 mt-1">
					<a href=""><img src="<?= base_url('img/porto1.jpg') ?>" class="img-fluid porto1"></a>
				</div>
				<div class="col col-sm-12 col-lg-5 mt-1">
					<a href=""><img src="<?= base_url('img/porto2.jpg') ?>" class="img-fluid porto2"></a>
				</div>
			</div>
			<div class="row d-flex flex-row justify-content-center">
				<div class="col col-sm-12 col-lg-5 mt-1">
					<a href=""><img src="<?= base_url('img/porto3.jpg') ?>" class="img-fluid porto3"></a>
				</div>
				<div class="col col-sm-12 col-lg-5 mt-1">
					<a href=""><img src="<?= base_url('img/porto4.jpg') ?>" class="img-fluid porto4"></a>
				</div>
			</div>
			<div class="row">
				<div class="col col-sm-12 mt-5 d-flex flex-row justify-content-center">
					<a href="login.html" class="btn btn-outline-dark px-4 radius">Lihat Selengkapnya</a>
				</div>
			</div>
		</div>
	</section>
	<!-- end portofolio -->
	<!-- layanan -->
	<section class="layanan py-5 bg-blacky mt-5">
		<div class="container">
			<h2 class="text-center text-white">LAYANAN KAMI</h2>
			<p class="text-center text-white tlayanan">Kepuasan klien bagi kami adalah nomor satu. Dengan begitu, kami akan selalu memberikan hasil yang sesuai ekspektasi di setiap proyek yang dikerjakan, dengan terus memperbarui layanan dan kemampuan kami.</p>
			<div class="row mt-5 d-flex flex-row">
				<div class="col-lg-4 col-12 mt-1">
					<img src="<?= base_url('img/layanan2.jpg') ?> " class="d-none d-lg-block img-fluid round img-lay">
					<h4 class="text-white mt-4">Pengembang Situs Web</h3>
					<p class="text-white mt-3 tlayanan">Siapa bilang situs web dan eCommerce haruslah mahal dan rumit? Disini, kami hanya fokus pada efektivitas yang akan menyesuaikan pada anggaran dan kebutuhan dari setiap klien kami.</p>
					<a href="login.html" class="btn btn-outline-light mt-3 px-4">Pelajari Selengkapnya</a>
				</div>
				<div class="col-lg-4 col-12 mt-1">
					<img src="<?= base_url('img/layanan3.jpg') ?> " class="d-none d-lg-block img-fluid round img-lay">
					<h4 class="text-white mt-4">Pengembang Aplikasi Web</h3>
					<p class="text-white mt-3 tlayanan">Kami mengembangkan aplikasi web pada berbagai platform open source untuk memastikan pengelolaan anggaran yang efektif.</p>
					<a href="login.html" class="btn btn-outline-light mt-3 px-4">Pelajari Selengkapnya</a>
				</div>
				<div class="col-lg-4 col-12 mt-1">
					<img src="<?= base_url('img/layanan.jpg') ?> " class="d-none d-lg-block img-fluid round img-lay">
					<h4 class="text-white mt-4">Pengembang Aplikasi Seluler</h3>
					<p class="text-white mt-3 tlayanan">Pengembangan aplikasi seluler yang mudah digunakan membantu berbagai startup dan perusahaan untuk mendefinisikan kembali pengalaman pengguna seluler.</p>
					<a href="login.html" class="btn btn-outline-light mt-3 px-4">Pelajari Selengkapnya</a>
				</div>
			</div>
		</div>
	</section>
	<!-- layanan -->
	<!-- keahlian -->
	<section>
		<div class="container">
			<div class="row d-flex flex-row align-items-center">
				<div class="col-sm-12 col-lg-6">
					<img src="<?= base_url('img/keahlian.jpg') ?>" class="img-fluid d-none d-lg-block">
				</div>
				<div class="col-sm-12 col-lg-6 py-5">
					<div class="row d-flex flex-row my-2">
						<div class="col-4">
							<img src="<?= base_url('img/B.png') ?>" class="img-fluid mb-1"> &nbsp;&nbsp; <span class="font-weight-bold mt-1">Keahlian Kami</span>
						</div>
						<div class="col-8">
							<h5>Kami percaya dengan dedikasi yang besar maka akan menciptakan hasil yang luar biasa.</h5>
							<p class="tkeahlian">Beberapa keahlian kami meliputi: Desain UI/UX, Riset UX, Desain dan Pengembangan Situs Web Responsif, Kampanye Microsite, Pengembangan Sistem Manajemen Konten (CMS), Pengembangan Platform eCommerce, Pengembangan Aplikasi Web Kustom, serta Pengembangan Aplikasi Seluler.</p>
							<div class="divider"></div>
						</div>
					</div>
					<div class="row d-flex flex-row my-2">
						<div class="col-4">
							<img src="<?= base_url('img/B.png') ?>" class="img-fluid mb-1"> &nbsp;&nbsp; <span class="font-weight-bold mt-1">Proses Kami</span>
						</div>
						<div class="col-8">
							<h5>Kami akan memberikan hasil yang sesuai ekspektasi Anda dengan mengerti dan memahami kebutuhan bisnis Anda.</h5>
							<p class="tkeahlian">Kami mampu memberikan layanan yang optimal, dengan menganalisis terlebih dahulu flow bisnis produk Anda untuk kemudian dilakukan rancangan strategi yang tepat, dan dieksekusi oleh tim yang berpengalaman dan profesional.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end keahlian -->
	<!-- testimoni -->
	<section class="py-5 bg-blacky">
		<div class="container d-flex flex-column justify-content-center align-items-center">
			<h2 class="text-center text-white">APA KATA CLIENT KAMI?</h2>
			<img src="<?= base_url('img/mangrove2.jpg') ?>" class="img-fluid img-round mx-auto mt-4" width="200">
			<p class="tlayanan text-center text-white mt-4">Perusahaan ini memiliki para developper yang handal dalam bidangnya. Saya sangat puas atas kinjera dari perusahaan ini. Produk yang dihasillkan sangatlah memuaskan. Mulai dari front-end sampai ke back-endnya sagat sesuai yang diharapkan</p>
			<h4 class="mt-5 text-center text-white">Bagaskara Dwi Putra - Pelanggan</h4>
			<a href="login.html" class="btn btn-danger px-4 mt-5 round"><b>PESAN SEKARANG</b></a>
		</div>
	</section>
	<!-- end testimoni -->
	<!-- konsultasi -->
	<section class="py-5">
		<div class="container">
			<div class="row d-flex flex-row align-items-center">
				<div class="col-lg-6 col-sm-12">
					<h1>KONSULTASI GRATIS</h1>
					<h3 class="mt-4">Anda telah datang jauh-jauh ke sini, mengapa tidak mengambil langkah pertama dan mengirim pesan kepada kami? Gratis!</h3>
					<p class="mt-4">Kami menawarkan konsultasi secara gratis tanpa persyaratan. Kami siap membantu untuk mengembangkan bisnis Anda di dunia digital. Hubungi kami dan tingkatkan bisnis Anda dengan efisien.</p>
				</div>
				<div class="col-sm-12 col-lg-6">
					<div class="row d-flex flex-row justify-content-center">
						<div class="col-6 mb-n-2">
							<form>
								<div class="form-group">
									<label for="nama">Nama</label>
    								<input type="email" class="form-control" id="nama">
								</div>
								<div class="form-group">
									<label for="telp">Telepon</label>
    								<input type="text" class="form-control" id="telp">
								</div>
						</div>
						<div class="col-6">
							<div class="form-group">
									<label for="email">Email</label>
    								<input type="email" class="form-control" id="email">
								</div>
								<div class="form-group">
									<label for="perusahaan">Perusahaan</label>
    								<input type="email" class="form-control" id="perusahaan">
								</div>
						</div>
					</div>
				  <div class="form-group">
						<label for="pesan">Pesan</label>
    					<input type="text" class="form-control textbesar" id="pesan">
				  </div>
				  <button type="submit" class="btn btn-outline-dark px-4">Submit</button>
				</form>
				</div>
			</div>
		</div>
	</section>
	<!-- end konsultasi -->
	<!-- footer -->
	<footer class="bg-blacky py-5">
		<div class="container text-white">
			<div class="row d-flex flex-row my-5">
				<div class="col-sm-12 col-lg-6">
					<img src="<?= base_url('img/logo-white.png') ?>" class="img-fluid">
					<p class="tfooter mt-3 mb-1">Address : Jl. Teuku Umar, Lampung</p>
					<p class="tfooter my-1">Phone : +6283121288450</p>
					<p class="tfooter">Email : bagaskara_dwi_putra@teknokrat.ac.id</p>
					<div class="row flex-row my-5">
						<div class="col-2">
							<a href="https://www.instagram.com/" target="_blank"><img src="<?= base_url('img/instagram.png') ?>" class="img-fluid"></a>
						</div>
						<div class="col-2">
							<a href="https://twitter.com/i/flow/login" target="_blank"><img src="<?= base_url('img/twitter.png') ?>" class="img-fluid"></a>
						</div>
						<div class="col-2">
							<a href="https://web.whatsapp.com/" target="_blank"><img src="<?= base_url('img/vector.png') ?>" class="img-fluid"></a>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-lg-3 mb-5">
					<a href=""><p class="tfooter font-weight-bold mt-2 link-footer">Information</p></a>
					<a href=""><p class="tfooter mt-4 mb-1 link-footer">About Us</p></a>
					<a href=""><p class="tfooter my-1 link-footer">Pricing</p></a>
					<a href=""><p class="tfooter my-1 link-footer">Contact</p></a>
					<a href=""><p class="tfooter my-1 link-footer">Services</p></a>				
				</div>
				<div class="col-sm-12 col-lg-3">
					<a href=""><p class="tfooter font-weight-bold mt-2 link-footer">Account</p></a>
					<a href=""><p class="tfooter mt-4 mb-1 link-footer">My Account</p></a>
					<a href=""><p class="tfooter my-1 link-footer">Contact</p></a>
					<a href=""><p class="tfooter my-1 link-footer">Shoping Cart</p></a>
					<a href=""><p class="tfooter my-1 link-footer">Shop</p></a>
				</div>
			</div>
		</div>
		<hr class="text-white">
		<p class="font-weight-bold text-white text-center mb-n5">Copyright © 2022 All rights reserved | <img src="<?= base_url('img/logo-white.png') ?>" class="img-fluid" width="80"></p>
	</footer>
	<!-- end footer -->

	<script src="<?php echo base_url('js/jquery.slim.min.js')?>"></script>
	<script src="<?php echo base_url('js/bootstrap.bundle.min.js')?>"></script>
	
</body>
</html>